<!doctype html>
<html lang="fr">
<head>
<meta charset="utf-8">
<title>Rectangle</title>

</head>
<body>
<?php 
// Inclusion de la classe Rectangle:
// require('EXO2_RectangleSoluce.php');

// Utilisation d'une fonction d'autoloading de classes
// Cette fonction trouve automatiquement la classe Rectangle
// PRE REQUIS: Existence d'un ficheir nommé rectangle.php
spl_autoload_extensions(".php"); // comma-separated list
spl_autoload_register();

// Definition longueur et largeur:
$longueur = 160;
$largeur = 75;


// Message bienvenue:
echo "<h2>Etude de rectangle de longueur $longueur et largeur $largeur</h2>";

// Création de rectangle:
$r = new Rectangle($longueur, $largeur);



// Impression aire.
echo '<p>Aire du rectangle ' . $r->recupereSurface() . '</p>';

// Recupere le perimetre.
echo '<p>Le perimetre du rectangle est ' . $r->recuperePerimetre() . '</p>';

// Est ce un carre?
echo '<p>Ce rectangle ';
if ($r->estCarre()) {
echo 'est aussi';
} else {
echo " n'est pas";
}
echo ' un carre.</p>';
// Destruction du rectangle:
unset($r);

?>
</body>
</html>