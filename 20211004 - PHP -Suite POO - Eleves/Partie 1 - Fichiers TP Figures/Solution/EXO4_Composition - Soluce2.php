<?php
class Adresse {
protected $ville;
protected $pays;

function __construct($uneVille="", $unPays="")
{
  $this->ville = $uneVille ;
  $this->pays = $unPays;
}

public function setVille($ville) {
  $this->ville = $ville;
  }
public function getVille() {
  return $this->ville; }

public function setPays($pays) {
  $this->pays = $pays;
  }
public function getPays() {
  return $this-> pays;
  }
}

class Personne {
protected $nom ;
public $adresse;


function __construct($unNom, $uneVille="", $unPays="")
{
  $this->nom = $unNom ;
  $this->adresse = new Adresse($uneVille, $unPays);
}

public function setNom($nom) {
    $this->nom = $nom;
  }
public function getNom() {
  return $this->nom;
  }

}

$timo = new Personne("Roberto","Maisons Alfort","France");

echo $timo->getNom() . ' vit à ' . $timo->adresse->getVille() . '.<BR>';

$roberta = new Personne("Roberta");

echo $roberta->getNom() . ' vit à ' . $roberta->adresse->getVille() . '.';


?>
