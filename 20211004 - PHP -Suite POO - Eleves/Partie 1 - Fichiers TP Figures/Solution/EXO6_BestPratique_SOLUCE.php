<?php

trait NomTrait {
private $nomLivre;

public function getNom() {
	return $this->nomLivre;
	}

public function setNom($leNom) {
	return $this->nomLivre = $leNom;
	}
}

interface NomInterface {
public function getNom();
public function setNom($unNom);
}

class Livre implements NomInterface {
	use NomTrait;
	
	public function affiche()
{
	echo "le nom du livre est" .$this->getNom()."<BR> " ;
}	
	
}

$monLivrePrefere= new Livre();
$monLivrePrefere->setNom("PHP 7 pour champions");
$monLivrePrefere->affiche() ;