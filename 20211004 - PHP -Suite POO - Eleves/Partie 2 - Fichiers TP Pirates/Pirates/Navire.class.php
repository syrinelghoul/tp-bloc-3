<?php

class Navire
{
    private $taille ;
    private $modele ;
    private $equipage = array() ;

    /**
     * @param $taille
     * @param $modele
     */
    public function __construct($taille, $modele)
    {
        $this->taille = $taille;
        $this->modele = $modele;
    }

    /**
     * @return mixed
     */
    public function getTaille()
    {
        return $this->taille;
    }

    /**
     * @param mixed $taille
     */
    public function setTaille($taille)
    {
        $this->taille = $taille;
    }

    /**
     * @return mixed
     */
    public function getModele()
    {
        return $this->modele;
    }

    /**
     * @param mixed $modele
     */
    public function setModele($modele)
    {
        $this->modele = $modele;
    }

    /**
     * @return array
     */
    public function getEquipage()
    {
        return $this->equipage;
    }

    /**
     * @param array $equipage
     */
    public function setEquipage($equipage)
    {
        $this->equipage = $equipage;
    }



    public function ajoutMarin(Marin $unMarin){
        $this->equipage[] = $unMarin ;
    }

    public function __toString()
    {
        // TODO: Implement __toString() method.
        $aAfficher = "La taille est: ".$this->getTaille().
            " et le modèle est: ".$this->getModele()."<BR>".
        "Les membres sont: ";

        if (! empty($this->getEquipage())) {
            foreach ($this->getEquipage() as $unMarin) {
                $aAfficher = $aAfficher . $unMarin;
            }
        }
        else $aAfficher = $aAfficher ."L' équipage est vide ou a été massacré par un pirate" ;
        return $aAfficher."<BR>";


    }


}