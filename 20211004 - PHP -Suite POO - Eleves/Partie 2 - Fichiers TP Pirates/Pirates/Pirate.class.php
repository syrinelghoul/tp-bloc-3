<?php

class Pirate extends Marin implements Piraterie
{
    private $degreSauvagerie ;

    /**
     * @return mixed
     */
    public function getDegreSauvagerie()
    {
        return $this->degreSauvagerie;
    }

    public function __construct($unParametreNom, $unParametreFonction, $unparametreDegreSauvegarie)
    {
        parent::__construct($unParametreNom, $unParametreFonction);
        $this->degreSauvagerie = $unparametreDegreSauvegarie ;

    }

    /**
     * @param mixed $degreSauvagerie
     */
    public function setDegreSauvagerie($degreSauvagerie)
    {
        $this->degreSauvagerie = $degreSauvagerie;
    }

    public function pillage(Navire $unNavire){
        // TODO: Implement pillage() method.
        if ($this->getDegreSauvagerie() >=50)
        {
            $unNavire->setEquipage(array());
        }
        else {
            $nouveauEquipage = array() ;
            foreach($unNavire->getEquipage() as $unmarin){
                if ($unmarin instanceof Capitaine){
                }
                else
                    $nouveauEquipage[] = $unmarin ;
            }
            $unNavire->setEquipage($nouveauEquipage);
        }
    }
}